/**
 * This is a modified version of ORBmatcher.cc from ORB-SLAM2 (see below).
 * Changes: 1) remove definition of some original member functions in class Converter; 
            2) add definition of function ORBmatcher::GetMatches.
 * Date: Nov 2019
 * Xi Lin, Dingyi Sun
 */

/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/

#include "ORBmatcher.h"

// #include<stdint-gcc.h>
#include <stdint.h>

using namespace std;

namespace ORB_SLAM2
{

const int ORBmatcher::TH_HIGH = 100;
const int ORBmatcher::TH_LOW = 50;
const int ORBmatcher::HISTO_LENGTH = 30;

ORBmatcher::ORBmatcher(float nnratio, bool checkOri): mfNNratio(nnratio), mbCheckOrientation(checkOri)
{
}

int ORBmatcher::GetMatches(const vector<cv::KeyPoint>& reference_kp, const DBoW2::FeatureVector& reference_FeatVec, const cv::Mat& reference_des, \
                            const cv::Mat& reference_ImDepth, const cv::Mat& reference_pose, const vector<cv::KeyPoint>& current_kp, const DBoW2::FeatureVector& current_FeatVec, \
                            const cv::Mat& current_des, const cv::Mat& current_ImDepth, const cv::Mat& current_pose, const cv::Mat& intrinsic,
                            pcl::PointCloud<pcl::PointXYZ>::Ptr& reference_pc, pcl::PointCloud<pcl::PointXYZ>::Ptr& current_pc)
{
    /** 
    * In this function we reference to ORB-SLAM2 by Raúl et al.
    * https://github.com/raulmur/ORB_SLAM2/blob/master/src/ORBmatcher.cc#L524-654
    **/
    
    vector<cv::Point2f> reference_matched_kp_1;
    vector<cv::Point2f> current_matched_kp_1;

    vector<cv::Point2f> reference_matched_kp_2;
    vector<cv::Point2f> current_matched_kp_2;

    vector<int> rotHist[HISTO_LENGTH];
    for(int i=0;i<HISTO_LENGTH;i++)
        rotHist[i].reserve(500);

    const float factor = 1.0f/HISTO_LENGTH;

    int nmatches = 0;

    DBoW2::FeatureVector::const_iterator f1it = reference_FeatVec.begin();
    DBoW2::FeatureVector::const_iterator f2it = current_FeatVec.begin();
    DBoW2::FeatureVector::const_iterator f1end = reference_FeatVec.end();
    DBoW2::FeatureVector::const_iterator f2end = current_FeatVec.end();

    while(f1it != f1end && f2it != f2end)
    {
        if(f1it->first == f2it->first)
        {
            for(size_t i1=0, iend1=f1it->second.size(); i1<iend1; i1++)
            {
                const size_t idx1 = f1it->second[i1];

                const cv::Mat &d1 = reference_des.row(idx1);

                int bestDist1=256;
                int bestIdx2 =-1 ;
                int bestDist2=256;

                for(size_t i2=0, iend2=f2it->second.size(); i2<iend2; i2++)
                {
                    const size_t idx2 = f2it->second[i2];

                    const cv::Mat &d2 = current_des.row(idx2);

                    int dist = DescriptorDistance(d1,d2);

                    if(dist<bestDist1)
                    {
                        bestDist2=bestDist1;
                        bestDist1=dist;
                        bestIdx2=idx2;
                    }
                    else if(dist<bestDist2)
                    {
                        bestDist2=dist;
                    }
                }

                if(bestDist1<TH_LOW)
                {
                    if(static_cast<float>(bestDist1)<mfNNratio*static_cast<float>(bestDist2))
                    {
                        reference_matched_kp_1.push_back(reference_kp[idx1].pt);
                        current_matched_kp_1.push_back(current_kp[bestIdx2].pt);
                        assert(reference_matched_kp_1.size() == current_matched_kp_1.size());
                        int match_id = reference_matched_kp_1.size()-1;

                        if(mbCheckOrientation)
                        {
                            float rot = reference_kp[idx1].angle-current_kp[bestIdx2].angle;
                            if(rot<0.0)
                                rot+=360.0f;
                            int bin = round(rot*factor);
                            if(bin==HISTO_LENGTH)
                                bin=0;
                            assert(bin>=0 && bin<HISTO_LENGTH);
                            rotHist[bin].push_back(match_id);
                        }
                        nmatches++;
                    }
                }
            }

            f1it++;
            f2it++;
        }
        else if(f1it->first < f2it->first)
        {
            f1it = reference_FeatVec.lower_bound(f2it->first);
        }
        else
        {
            f2it = current_FeatVec.lower_bound(f1it->first);
        }
    }

    if(mbCheckOrientation)
    {
        int ind1=-1;
        int ind2=-1;
        int ind3=-1;

        ComputeThreeMaxima(rotHist,HISTO_LENGTH,ind1,ind2,ind3);

        for(int i=0; i<HISTO_LENGTH; i++)
        {
            if(i==ind1 || i==ind2 || i==ind3){
                for(size_t j=0, jend=rotHist[i].size(); j<jend; j++)
                {
                    reference_matched_kp_2.push_back(reference_matched_kp_1[rotHist[i][j]]);
                    current_matched_kp_2.push_back(current_matched_kp_1[rotHist[i][j]]);
                    assert(reference_matched_kp_2.size() == current_matched_kp_2.size());
                }
            }
            else{
                for(size_t j=0, jend=rotHist[i].size(); j<jend; j++)
                    nmatches--;
            }
        }
    }
    else{
        reference_matched_kp_2 = reference_matched_kp_1;
        current_matched_kp_2 = current_matched_kp_1;
    }
    
    if(nmatches < 4){
       return nmatches;
    }

    cv::Mat mask;
    cv::Mat homography = cv::findHomography(reference_matched_kp_2, current_matched_kp_2, CV_RANSAC, 3.0, mask);

    int final_matches = 0;

    // intrinsic
    float fx = intrinsic.at<float>(0,0);
    float fy = intrinsic.at<float>(1,1);
    float cx = intrinsic.at<float>(0,2);
    float cy = intrinsic.at<float>(1,2);
    
    for(std::vector<cv::Point2f>::size_type i = 0; i < reference_matched_kp_2.size(); i++)
    {      
        if((unsigned int)mask.at<uchar>(i)){
            float r_dep = reference_ImDepth.at<float>(reference_matched_kp_2[i].y,reference_matched_kp_2[i].x);
            float c_dep = current_ImDepth.at<float>(current_matched_kp_2[i].y,current_matched_kp_2[i].x);
            if(r_dep > 0 && c_dep > 0){
                pcl::PointXYZ r_point;
                r_point.x = (reference_matched_kp_2[i].x - cx) * r_dep / fx;
                r_point.y = (reference_matched_kp_2[i].y - cy) * r_dep / fy;
                r_point.z = r_dep; 

                reference_pc->points.push_back(r_point);

                pcl::PointXYZ c_point;
                c_point.x = (current_matched_kp_2[i].x - cx) * c_dep / fx;
                c_point.y = (current_matched_kp_2[i].y - cy) * c_dep / fy;
                c_point.z = c_dep;

                current_pc->points.push_back(c_point);

                final_matches++;
            }
        }

    }

    return final_matches;
} 

void ORBmatcher::ComputeThreeMaxima(vector<int>* histo, const int L, int &ind1, int &ind2, int &ind3)
{
    int max1=0;
    int max2=0;
    int max3=0;

    for(int i=0; i<L; i++)
    {
        const int s = histo[i].size();
        if(s>max1)
        {
            max3=max2;
            max2=max1;
            max1=s;
            ind3=ind2;
            ind2=ind1;
            ind1=i;
        }
        else if(s>max2)
        {
            max3=max2;
            max2=s;
            ind3=ind2;
            ind2=i;
        }
        else if(s>max3)
        {
            max3=s;
            ind3=i;
        }
    }

    if(max2<0.1f*(float)max1)
    {
        ind2=-1;
        ind3=-1;
    }
    else if(max3<0.1f*(float)max1)
    {
        ind3=-1;
    }
}


// Bit set count operation from
// http://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetParallel
int ORBmatcher::DescriptorDistance(const cv::Mat &a, const cv::Mat &b)
{
    const int *pa = a.ptr<int32_t>();
    const int *pb = b.ptr<int32_t>();

    int dist=0;

    for(int i=0; i<8; i++, pa++, pb++)
    {
        unsigned  int v = *pa ^ *pb;
        v = v - ((v >> 1) & 0x55555555);
        v = (v & 0x33333333) + ((v >> 2) & 0x33333333);
        dist += (((v + (v >> 4)) & 0xF0F0F0F) * 0x1010101) >> 24;
    }

    return dist;
}

} //namespace ORB_SLAM
