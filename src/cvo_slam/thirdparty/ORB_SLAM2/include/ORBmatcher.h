/**
 * This is a modified version of ORBmatcher.h from ORB-SLAM2 (see below).
 * Changes: 1) remove some original member functions in class ORBmatcher; 
            2) remove some original header files;
            3) add a new function ORBmatcher::GetMatches;
            4) add PCL library header files;
 * Date: Nov 2019
 * Xi Lin, Dingyi Sun
 */

/**
* This file is part of ORB-SLAM2.
*
* Copyright (C) 2014-2016 Raúl Mur-Artal <raulmur at unizar dot es> (University of Zaragoza)
* For more information see <https://github.com/raulmur/ORB_SLAM2>
*
* ORB-SLAM2 is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* ORB-SLAM2 is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with ORB-SLAM2. If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ORBMATCHER_H
#define ORBMATCHER_H

#include<vector>
#include<opencv2/core/core.hpp>
#include<opencv2/features2d/features2d.hpp>
#include<opencv2/calib3d.hpp>

#include<limits.h>

#include <opencv2/core/core.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <pcl/point_cloud.h>
#include <pcl/point_types.h>

#include "Thirdparty/DBoW2/DBoW2/FeatureVector.h"

#include <algorithm> 


namespace ORB_SLAM2
{

class ORBmatcher
{    
public:

    ORBmatcher(float nnratio=0.6, bool checkOri=true);

    // Computes the Hamming distance between two ORB descriptors
    static int DescriptorDistance(const cv::Mat &a, const cv::Mat &b);
    
    int GetMatches(const std::vector<cv::KeyPoint>& reference_kp, const DBoW2::FeatureVector& reference_FeatVec, const cv::Mat& reference_des, \
                    const cv::Mat& reference_ImDepth, const cv::Mat& reference_pose, const std::vector<cv::KeyPoint>& current_kp, const DBoW2::FeatureVector& current_FeatVec, \
                    const cv::Mat& current_des, const cv::Mat& current_ImDepth, const cv::Mat& current_pose, const cv::Mat& intrinsic,
                    pcl::PointCloud<pcl::PointXYZ>::Ptr& reference_pc, pcl::PointCloud<pcl::PointXYZ>::Ptr& current_pc);

public:

    static const int TH_LOW;
    static const int TH_HIGH;
    static const int HISTO_LENGTH;


protected:

    void ComputeThreeMaxima(std::vector<int>* histo, const int L, int &ind1, int &ind2, int &ind3);

    float mfNNratio;
    bool mbCheckOrientation;
};

}// namespace ORB_SLAM

#endif // ORBMATCHER_H
