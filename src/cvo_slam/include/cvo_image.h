/* ----------------------------------------------------------------------------
 * Copyright 2019, Xi Lin <bexilin@umich.edu>, Dingyi Sun <dysun@umich.edu>
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

#ifndef CVO_IMAGE_H
#define CVO_IMAGE_H

#include <Eigen/Geometry>
#include <Eigen/Core>
#include <Eigen/Dense>
#include <opencv2/opencv.hpp>
#include <boost/shared_ptr.hpp>
#include <string>
#include <ros/time.h>

namespace cvo_slam{

typedef Eigen::Affine3d Transform; // data type for pose estimation
typedef Eigen::Matrix<double, 6, 6> Information; // data type for information matrix
typedef Eigen::Isometry3d Transform_Vertex; // data type for pose estimation in g2o
typedef Eigen::Matrix3d Rotation; // data type for rotation part of pose estimation

struct Image
{
    cv::Mat rgb;
    cv::Mat depth;
    ros::Time timestamp;
};

typedef boost::shared_ptr<Image> Image_Ptr;

} // namespace cvo_slam

#endif // CVO_IMAGE_H