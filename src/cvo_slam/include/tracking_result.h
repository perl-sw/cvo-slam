/* ----------------------------------------------------------------------------
 * Copyright 2019, Xi Lin <bexilin@umich.edu>, Dingyi Sun <dysun@umich.edu>
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

#ifndef TRACKING_RESULT_H
#define TRACKING_RESULT_H

#include "cvo_image.h"
#include <iostream>
#include <g2o/core/optimizable_graph.h>
#include "rkhs_se3.hpp"

namespace cvo_slam
{

class tracking_result : public g2o::OptimizableGraph::Data
{
public:
    Transform transform;
    // Transform prior;
    Information information;

    cvo::inn_p inn_pre; // innerproduct result of Identitiy transform
    cvo::inn_p inn_post; // innerproduct result of transformation after CVO registration
    cvo::inn_p inn_prior; // innerproduct result of relative trasformation between the pose estimation of two frames 
    cvo::inn_p inn_lc_prior; // innerproduct result of transformation given by indirect loop-closure detection

   
    int matches;  // number of matched features between two images
    float score; // similarity score between two images

    bool isNaN() const
    {
        return !std::isfinite(transform.matrix().sum()) || !std::isfinite(information.sum());
    }

    tracking_result()
    {
    }
    tracking_result(const tracking_result &result) : transform(result.transform),
                                                    //  prior(result.prior),
                                                     information(result.information),
                                                     inn_pre(result.inn_pre),
                                                     inn_post(result.inn_post),
                                                     inn_prior(result.inn_prior),
                                                     inn_lc_prior(result.inn_lc_prior),
                                                     matches(result.matches),
                                                     score(result.score)
    {
    }

    virtual bool read(std::istream &is)
    {
        return true;
    }

    virtual bool write(std::ostream &os) const
    {
        return false;
    }

private:
};


} // namespace cvo_slam

#endif // TRACKING_RESULT_H