/* ----------------------------------------------------------------------------
 * Copyright 2019, Xi Lin <bexilin@umich.edu>, Dingyi Sun <dysun@umich.edu>
 * All Rights Reserved
 * See LICENSE for the license information
 * -------------------------------------------------------------------------- */

#ifndef KEYFRAME_H_
#define KEYFRAME_H_

#include "cfg.h"
#include "cvo_image.h"

#include "Thirdparty/DBoW2/DBoW2/BowVector.h"
#include "Thirdparty/DBoW2/DBoW2/FeatureVector.h"
#include "ORBVocabulary.h"
#include "ORBextractor.h"

#include <ros/time.h>
#include <Eigen/Geometry>
#include <vector>

namespace cvo_slam
{

class Keyframe
{
public:

  Keyframe(const Image_Ptr& image_, const Transform& pose_, const cv::Mat& intrinsic_, bool mbRGB, float mDepthMapFactor):
  image(image_), ImGray(image_->rgb), ImDepth(image_->depth), timestamp(image_->timestamp), pose(pose_), intrinsic(intrinsic_)
  {
    /** 
    * In this function we reference to ORB-SLAM2 by Raúl et al.
    * https://github.com/raulmur/ORB_SLAM2/blob/master/src/Tracking.cc#L212-232
    **/
    
    if(ImGray.channels()==3)
    {
        if(mbRGB)
            cvtColor(ImGray,ImGray,CV_RGB2GRAY);
        else
            cvtColor(ImGray,ImGray,CV_BGR2GRAY);
    }
    else if(ImGray.channels()==4)
    {
        if(mbRGB)
            cvtColor(ImGray,ImGray,CV_RGBA2GRAY);
        else
            cvtColor(ImGray,ImGray,CV_BGRA2GRAY);
    }

    if((fabs(mDepthMapFactor-1.0f)>1e-5) || ImDepth.type()!=CV_32F)
        ImDepth.convertTo(ImDepth,CV_32F,mDepthMapFactor);
  }

  ~Keyframe() {};

public:

  int id;
  cv::Mat ImGray;
  cv::Mat ImDepth;
  ros::Time timestamp;
  Transform pose;

  // Used to compute cvo inner product in loop closure
  Image_Ptr image;

  // Keypoints, descriptors 
  std::vector<cv::KeyPoint> keypoints;
  cv::Mat descriptors;

  // Feature and BoW vector
  DBoW2::BowVector BowVec;
  DBoW2::FeatureVector FeatVec;

  // Camera intrinsic
  cv::Mat intrinsic;
};

typedef boost::shared_ptr<Keyframe> KeyframePtr;
typedef std::vector<KeyframePtr> KeyframeVector;

} // namespace cvo_slam

#endif // KEYFRAME_H_
