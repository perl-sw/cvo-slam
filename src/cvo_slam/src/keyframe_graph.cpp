/**
 * This is a modified version of keyframe_graph.cpp from dvo (see below).
 * Changes: 1) add definition of new functions in class KeyframeGraph; 
 *          2) remove some original member functions and variables in class KeyframeGraphImpl; 
 *          3) In class KeyframeGraphImpl, claim new member functions and variables, and add function definitions; 
 *          4) change the namespace from dvo_slam to cvo_slam
 * Date: Nov 2019
 * Xi Lin, Dingyi Sun
 */

/**
 *  This file is part of dvo.
 *
 *  Copyright 2012 Christian Kerl <christian.kerl@in.tum.de> (Technical University of Munich)
 *  For more information see <http://vision.in.tum.de/data/software/dvo>.
 *
 *  dvo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  dvo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with dvo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "keyframe_graph.h"


namespace cvo_slam
{

namespace internal
{

struct FindEdgeById
{
public:
  FindEdgeById(int id) : id_(id)
  {
  }

  bool operator()(g2o::HyperGraph::Edge *e)
  {
    return e->id() == id_;
  }

private:
  int id_;
};

static cvo_slam::Transform_Vertex toIsometry(const cvo_slam::Transform &pose)
{
  cvo_slam::Transform_Vertex p(pose.rotation());
  p.translation() = pose.translation();

  return p;
}

static cvo_slam::Transform toAffine(const cvo_slam::Transform_Vertex &pose)
{
  cvo_slam::Transform p(pose.rotation());
  p.translation() = pose.translation();

  return p;
}

static inline int combine(const short &left, const short &right)
{
  int lower, upper;

  if (left < right)
  {
    lower = right;
    upper = left;
  }
  else
  {
    lower = left;
    upper = right;
  }

  return upper << 16 | lower;
}

class KeyframeGraphImpl
{
public:
  friend class ::cvo_slam::KeyframeGraph;
  static const int FirstOdometryId = 1 << 30;
  KeyframeGraphImpl(OrbVocabularyPtr& OrbVoc_) :
                        OrbVoc(OrbVoc_),
                        lc_num(0),
                        optimization_thread_shutdown_(false),
                        optimization_thread_(boost::bind(&KeyframeGraphImpl::execOptimization, this)),
                        next_keyframe_id_(1),
                        next_odometry_vertex_id_(FirstOdometryId),
                        next_odometry_edge_id_(FirstOdometryId)
  {
    // initialize orb matcher
    OrbMatcher.reset(new ORB_SLAM2::ORBmatcher(cfg_.LC_MatchThreshold,true));
    
    // g2o version used here is 20170730 (https://github.com/RainerKuemmerle/g2o/releases), can not use a newer version 
    keyframegraph_.setAlgorithm(
        new g2o::OptimizationAlgorithmDogleg(
            new BlockSolver(
                new LinearSolver())));

    keyframegraph_.setVerbose(false);

    configure(cfg_);
  }

  ~KeyframeGraphImpl()
  {
    optimization_thread_shutdown_ = true;
    new_keyframes_.push(LocalMap::Ptr());
    optimization_thread_.join();
  }

  void configure(const cvo_slam::cfg &cfg)
  {
    cfg_ = cfg;
  }

  void add(const LocalMap::Ptr &keyframe)
  {
    if (cfg_.UseMultiThreading)
    {
      new_keyframes_.push(keyframe);
    }
    else
    {
      // wait until queue is empty
      tbb::mutex::scoped_lock l(queue_empty_sync_);

      newKeyframe(keyframe);
    }
  }

  void finalOptimization()
  {

    tbb::mutex::scoped_lock l;
    std::cerr << "final optimization, waiting for all keyframes";
    while (!l.try_acquire(queue_empty_sync_))
    {
      std::cerr << ".";
      tbb::this_tbb_thread::sleep(tbb::tick_count::interval_t(0.1));
    }
    std::cerr << std::endl;

    std::cerr << keyframes_.size() << " keyframes" << std::endl;

    // include all edges in the optimization
    if (cfg_.FinalOptimizationUseDenseGraph && !cfg_.OptimizationUseDenseGraph)
    {
      for (g2o::OptimizableGraph::EdgeSet::iterator e_it = keyframegraph_.edges().begin(); e_it != keyframegraph_.edges().end(); ++e_it)
      {
        g2o::EdgeSE3 *e = (g2o::EdgeSE3 *)(*e_it);
        e->setLevel(0);
      }
    }

    std::cerr << "optimizing..." << std::endl;

    keyframegraph_.setVerbose(true);
    keyframegraph_.initializeOptimization(0);
    keyframegraph_.optimize(cfg_.FinalOptimizationIterations);


    std::cerr << "done" << std::endl;

    // update keyframe database
    updateKeyframePosesFromGraph();

    map_changed_(*me_);

    // If you want to print edge data, enable the following two lines
    // G2oPrint();
    // KeyframeEdgesPrint();
  }

private:
  typedef g2o::BlockSolver_6_3 BlockSolver;
  typedef g2o::LinearSolverEigen<BlockSolver::PoseMatrixType> LinearSolver;

  void execOptimization()
  {

    bool is_locked = false;

    while (!optimization_thread_shutdown_)
    {
      LocalMap::Ptr new_keyframe;

      new_keyframes_.pop(new_keyframe);

      if (new_keyframe)
      {
        if (!is_locked)
        {
          queue_empty_sync_.lock();
          is_locked = true;
        }

        newKeyframe(new_keyframe);

        if (is_locked && new_keyframes_.empty())
        {
          queue_empty_sync_.unlock();
          is_locked = false;
        }
      }
    }
  }

  void newKeyframe(const LocalMap::Ptr &map)
  {
    tbb::mutex::scoped_lock l(new_keyframe_sync_);

    // Insert current local pose graph into the global pose graph
    KeyframePtr keyframe = insertNewKeyframe(map);

    // Early abort
    if (keyframes_.size() <= 2){
      return;
    }

    // Perform indirect loop-closure detection via ORB features and direct transformation estimation with CVO 
    int new_lc = detectLoopClousure(keyframe);

    lc_num += new_lc;

    std::cout << std::endl << "All loop closure constraint numbers: " << lc_num << std::endl << std::endl;

    if(new_lc >= cfg_.MinConstraintDistance)
    {
      // Perform keyframe pose graph optimization
      keyframegraph_.initializeOptimization(0);
      keyframegraph_.optimize(cfg_.OptimizationIterations);

      // Update keyframe poses after optimization
      updateKeyframePosesFromGraph();
    }

    // if this is the last local map, set last frame to keyframe, and do loop-closure detection and keyframe pose graph optimization
    if(map->lastMap()){
      KeyframePtr keyframe = insertLastKeyframe(map);

      int new_lc = detectLoopClousure(keyframe);

      lc_num += new_lc;

      std::cout << "\n\n\n" << "Number of loop closure constraint: " << lc_num << "\n\n\n";

      if(new_lc >= cfg_.MinConstraintDistance)
      {
        keyframegraph_.initializeOptimization(0);
        keyframegraph_.optimize(cfg_.OptimizationIterations);

        updateKeyframePosesFromGraph();
      }
    }

    map_changed_(*me_);
  }

  void insertConstraint(const KeyframePtr &keyframe, const KeyframePtr &constraint, const cvo_slam::tracking_result &result)
  {
    int edge_id = combine(constraint->id, keyframe->id);

    g2o::EdgeSE3 *e = new g2o::EdgeSE3();
    e->setId(edge_id);

    e->setMeasurement(toIsometry(result.transform));

    e->setRobustKernel(createRobustKernel());
    e->setInformation(result.information);
    e->resize(2);

    e->setLevel(0);
    e->setUserData(new cvo_slam::tracking_result(result));

    e->setVertex(0, keyframegraph_.vertex(keyframe->id));
    e->setVertex(1, keyframegraph_.vertex(constraint->id));

    keyframegraph_.addEdge(e);
  }

  void updateKeyframePosesFromGraph()
  {
    for (KeyframeVector::iterator it = keyframes_.begin(); it != keyframes_.end(); ++it)
    {
      const KeyframePtr &keyframe = *it;

      g2o::VertexSE3 *vertex = (g2o::VertexSE3 *)keyframegraph_.vertex(keyframe->id);

      keyframe->pose = toAffine(vertex->estimate());
    }
  }

  struct FindEdge
  {
    int id1, id2;

    FindEdge(int id1, int id2) : id1(id1), id2(id2)
    {
    }

    bool operator()(const g2o::HyperGraph::Edge *e) const
    {
      return e->vertices().size() == 2 && ((e->vertex(0)->id() == id1 && e->vertex(1)->id() == id2) || (e->vertex(1)->id() == id1 && e->vertex(0)->id() == id2));
    }
  };

  void addGraph(g2o::OptimizableGraph *g)
  {
    // Add vertices in a local pose graph to the global pose graph
    for (g2o::HyperGraph::VertexIDMap::iterator it = g->vertices().begin(); it != g->vertices().end(); ++it)
    {
      g2o::OptimizableGraph::Vertex *v = (g2o::OptimizableGraph::Vertex *)(it->second);
      if (keyframegraph_.vertex(v->id()))
        continue;

      g2o::VertexSE3 *v1 = (g2o::VertexSE3 *)v;
      g2o::VertexSE3 *v2 = new g2o::VertexSE3();
      v2->setId(v1->id());
      v2->setEstimate(v1->estimate());
      v2->setMarginalized(v1->marginalized());
      v2->setUserData(v1->userData());
      v1->setUserData(0);
      v2->setHessianIndex(-1);

      if (!keyframegraph_.addVertex(v2))
      {
        throw std::runtime_error("failed to add vertex to g2o graph!");
      }
    }

    // Add edges in a local pose graph to the global pose graph, and apply Cauchy robust kernel to them 
    for (g2o::HyperGraph::EdgeSet::iterator it = g->edges().begin(); it != g->edges().end(); ++it)
    {
      g2o::EdgeSE3 *e = (g2o::EdgeSE3 *)(*it);
      g2o::EdgeSE3 *en = new g2o::EdgeSE3();

      en->setId(e->id());
      en->setLevel(e->level());
      en->setRobustKernel(createRobustKernel());
      en->setMeasurement(e->measurement());
      en->setInformation(e->information());
      en->setUserData(e->userData());
      e->setUserData(0);
      en->resize(e->vertices().size());
      int cnt = 0;
      for (std::vector<g2o::HyperGraph::Vertex *>::const_iterator it = e->vertices().begin(); it != e->vertices().end(); ++it)
      {
        g2o::OptimizableGraph::Vertex *v = (g2o::OptimizableGraph::Vertex *)keyframegraph_.vertex((*it)->id());
        assert(v);
        en->setVertex(cnt++, v);
      }
      keyframegraph_.addEdge(en);
    }
  }

  int detectLoopClousure(KeyframePtr& reference)
  {
    int new_lc_num = 0;
    std::cout << std::endl << "Performing loop-closure detection" << std::endl << std::endl;
    KeyframePtr last_keyframe = keyframes_[keyframes_.size()-2];

    // The given threshold of normalized similarity score is applied here to get the minimum acceptable similarity score  
    float min_score = cfg_.LC_MinScoreRatio * OrbVoc->score(reference->BowVec, last_keyframe->BowVec);

    for(KeyframeVector::iterator it = keyframes_.begin(); it != keyframes_.end(); it++)
    {
      if((*it)->id == reference->id || std::abs((*it)->id - reference->id) == 1) continue;

      tracking_result result;

      result.score = OrbVoc->score(reference->BowVec, (*it)->BowVec);

      // Reject loop-closure if similarity score is lower than minimum acceptable value
      if(result.score < min_score){
        // std::cout << std::endl;
        continue;
      }

      // std::cout << "\nkeyframe " << (*it)->id << " score: " << result.score << std::endl;
      
      cv::Mat reference_pose = ORB_SLAM2::Converter::toCvMat(reference->pose);
      cv::Mat current_pose = ORB_SLAM2::Converter::toCvMat((*it)->pose);

      pcl::PointCloud<pcl::PointXYZ>::Ptr reference_pc(new pcl::PointCloud<pcl::PointXYZ>());
      pcl::PointCloud<pcl::PointXYZ>::Ptr current_pc(new pcl::PointCloud<pcl::PointXYZ>());

      // Match ORB features and get corresponding point clouds 
      int matches = OrbMatcher->GetMatches(reference->keypoints, reference->FeatVec, reference->descriptors, reference->ImDepth, \
                                           reference_pose, (*it)->keypoints, (*it)->FeatVec, (*it)->descriptors, (*it)->ImDepth, \
                                           current_pose, reference->intrinsic, reference_pc, current_pc);

      // Reject loop-closure if number of matched features is lower than the given threshold
      if(matches < cfg_.LC_MinMatch){
        // std::cout << std::endl;
        continue;
      }

      result.matches = matches;
      
      Eigen::Matrix<double,4,4> T_cr;

      // Compute initial transformation estimation through SVD
      PointCloudRegistrator.estimateRigidTransformation(*current_pc, *reference_pc, T_cr);

      Eigen::Affine3f prior = ((reference->pose).inverse() * ((*it)->pose)).cast<float>();

      Transform lc_constraint = ORB_SLAM2::Converter::toAffine(T_cr);
      Eigen::Affine3f lc_prior = lc_constraint.cast<float>();


      cvo::rkhs_se3 cvo;

      // Set the initial value of CVO to be initial transformation estimation
      cvo.reset_initial(lc_prior);

      cvo.set_pcd(cfg_.dataset_seq, reference->image->rgb, reference->image->depth);

      cvo.match_keyframe(cfg_.dataset_seq, (*it)->image->rgb, (*it)->image->depth, result.transform);
      Eigen::Affine3f lc_post = result.transform.cast<float>();

      cvo.compute_innerproduct_lc(result.inn_prior, result.inn_lc_prior, result.inn_pre, result.inn_post, prior, lc_prior, lc_post);

      // Reject loop-closure if the innerproduct of transformation estimation of CVO is not larger than that of any other transformations  
      if ((result.inn_post.value <= result.inn_pre.value) || (result.inn_post.value <= result.inn_lc_prior.value) 
          || (result.inn_post.value <= result.inn_prior.value)){
        // std::cout << std::endl;
        continue;
      }

      std::cout << "Accept loop-closure between keyframe " << reference->id << " and " << (*it)->id << std::endl;

      // Currently we do not have effective way to estimate information of a loop-closure measurement, so set the information as a large and uniform value 
      result.information = 500000 * Information::Identity();

      insertLoopClosureConstraint(reference, *it, result);

      new_lc_num++;
      
      std::cout << std::endl;
    }
    std::cout << std::endl;
    return new_lc_num;
  }

  void insertLoopClosureConstraint(const KeyframePtr &reference, const KeyframePtr &current, const tracking_result &result)
  {
    int edge_id = combine(current->id, reference->id);

    g2o::EdgeSE3 *e = new g2o::EdgeSE3();
    e->setId(edge_id);

    e->setMeasurement(toIsometry(result.transform));

    // Add Cauchy robust kernel to loop-closure edges
    e->setRobustKernel(createRobustKernel());
    
    e->setInformation(result.information);
    e->resize(2);

    e->setLevel(0);
    e->setUserData(new cvo_slam::tracking_result(result));

    e->setVertex(0, keyframegraph_.vertex(reference->id));
    e->setVertex(1, keyframegraph_.vertex(current->id));

    keyframegraph_.addEdge(e);
  }

  KeyframePtr insertNewKeyframe(const LocalMap::Ptr &m)
  {
    // Set the pose of current keyframe vertex, which is also the last frame in the last local pose graph, with 
    // that of last keyframe vertex and transformation between them 
    if (!keyframes_.empty())
    {
      g2o::HyperGraph::Vertex *last_kv_tmp = keyframegraph_.vertex(next_keyframe_id_ - 1);

      if (last_kv_tmp == 0)
      {
        throw std::runtime_error("last_kv_tmp == nullptr");
      }

      g2o::VertexSE3 *last_kv = dynamic_cast<g2o::VertexSE3 *>(last_kv_tmp);
      g2o::OptimizableGraph::EdgeSet::iterator e = std::find_if(last_kv->edges().begin(), last_kv->edges().end(), FindEdge(next_keyframe_id_ - 1, next_odometry_vertex_id_));

      assert(e != last_kv->edges().end());

      g2o::EdgeSE3 *e_se3 = (g2o::EdgeSE3 *)(*e);

      m->setKeyframePose(toAffine(last_kv->estimate() * e_se3->measurement()));
    }

    // Perform local pose graph optimization
    m->optimize();

    g2o::SparseOptimizer &g = m->getGraph();

    int max_id = g.vertices().size();

    g2o::OptimizableGraph::VertexIDMap vertices = g.vertices();
    for (g2o::OptimizableGraph::VertexIDMap::iterator v_it = vertices.begin(); v_it != vertices.end(); ++v_it)
    {
      g.changeId(v_it->second, next_odometry_vertex_id_ + (v_it->second->id() - 1));
    }

    for (g2o::OptimizableGraph::EdgeSet::iterator e_it = g.edges().begin(); e_it != g.edges().end(); ++e_it)
    {
      g2o::EdgeSE3 *e = (g2o::EdgeSE3 *)(*e_it);
      e->setId(next_odometry_edge_id_++);
      e->setLevel(cfg_.OptimizationUseDenseGraph ? 0 : 2);
    }

    // Add current local pose graph to global pose graph 
    addGraph(&g);

    // Change the vertex type of current keyframe vertex from a normal frame vertex to a keyframe vertex
    g2o::VertexSE3 *kv = (g2o::VertexSE3 *)keyframegraph_.vertex(next_odometry_vertex_id_);
    if (kv == 0)
    {
      throw std::runtime_error("kv == nullptr");
    }
    if (!keyframegraph_.changeId(kv, next_keyframe_id_))
    {
      throw std::runtime_error("keyframegraph_.changeId(kv, next_keyframe_id_) failed!");
    }

    if (!keyframes_.empty())
    {
      // Find the edge between last keyframe vertex and current keyframe vertex 
      g2o::OptimizableGraph::EdgeSet::iterator ke = std::find_if(kv->edges().begin(), kv->edges().end(), FindEdge(next_keyframe_id_ - 1, next_keyframe_id_));

      assert(ke != kv->edges().end());

      // Change the edge type from a normal edge to a keyframe edge
      g2o::OptimizableGraph::Edge *e = (g2o::OptimizableGraph::Edge *)(*ke);
      e->setId(combine(next_keyframe_id_ - 1, next_keyframe_id_));
      e->setLevel(0);
    }
    else
    {
      kv->setFixed(true);
    }

    KeyframePtr keyframe = m->getKeyframe();

    keyframe->id = next_keyframe_id_;

    kv->setUserData(new cvo_slam::Timestamped(keyframe->timestamp));

    keyframes_.push_back(keyframe);

    // Increment ids
    next_odometry_vertex_id_ += max_id - 1;
    next_keyframe_id_ += 1;

    return keyframe;
  }

  KeyframePtr insertLastKeyframe(const LocalMap::Ptr &m)
  {
    // Change the vertex type of final frame vertex from a normal frame vertex to a keyframe vertex
    g2o::VertexSE3 *kv = (g2o::VertexSE3 *)keyframegraph_.vertex(next_odometry_vertex_id_);
    if (kv == 0)
    {
      throw std::runtime_error("kv == nullptr");
    }
    if (!keyframegraph_.changeId(kv, next_keyframe_id_))
    {
      throw std::runtime_error("keyframegraph_.changeId(kv, next_keyframe_id_) failed!");
    }

    // Find the edge between final frame vertex and current keyframe vertex
    g2o::OptimizableGraph::EdgeSet::iterator ke = std::find_if(kv->edges().begin(), kv->edges().end(), FindEdge(next_keyframe_id_ - 1, next_keyframe_id_));

    assert(ke != kv->edges().end());

    // Change the edge type from a normal edge to a keyframe edge
    g2o::OptimizableGraph::Edge *e = (g2o::OptimizableGraph::Edge *)(*ke);
    e->setId(combine(next_keyframe_id_ - 1, next_keyframe_id_));
    e->setLevel(0);

    KeyframePtr last_keyframe = m->getLastKeyframe();

    last_keyframe->id = next_keyframe_id_;

    kv->setUserData(new cvo_slam::Timestamped(last_keyframe->timestamp));

    keyframes_.push_back(last_keyframe);

    return last_keyframe;
  }

  g2o::RobustKernel *createRobustKernel()
  {
    if (cfg_.UseRobustKernel)
    {
      g2o::RobustKernel *k = new g2o::RobustKernelCauchy();
      k->setDelta(cfg_.RobustKernelDelta);

      return k;
    }
    else
    {
      return 0;
    }
  }

  // Debug function for printing the data of all non loop-closure edges in the global pose graph, not used by default 
  void G2oPrint()
  {
    std::ofstream g2o_file;

    // Modify the address to your local address
    g2o_file.open("/home/xi/edges_no_lc.txt");

    double vertex_pose[7];
    double edge_pose[7];
    bool abandon = false;
    int pre_id;

    g2o::EdgeSE3::InformationType edge_information;
    
    for (g2o::OptimizableGraph::EdgeSet::iterator iter = keyframegraph_.edges().begin(); iter != keyframegraph_.edges().end(); ++iter)
    {
      g2o::EdgeSE3 *it = (g2o::EdgeSE3 *)(*iter);
      std::stringstream vertices_id;

      pre_id = 0;
      g2o::VertexSE3 *temp_v1 = (g2o::VertexSE3 *)it->vertex(0);
      g2o::VertexSE3 *temp_v2 = (g2o::VertexSE3 *)it->vertex(1);

      if ((temp_v1->id() < FirstOdometryId) && (temp_v2->id() < FirstOdometryId) && (abs(temp_v1->id() - temp_v2->id()) != 1))
      {
          continue;
      }

      for (g2o::HyperGraph::VertexContainer::iterator i = (it->vertices()).begin(); i != (it->vertices()).end(); ++i)
      {
        pre_id = (*i)->id();
        vertices_id << (*i)->id() - 1 << " ";
      }

      vertices_id << ((cvo_slam::Timestamped *)((temp_v1)->userData()))->timestamp << " " << ((cvo_slam::Timestamped *)((temp_v2)->userData()))->timestamp << " ";


      g2o_file << vertices_id.str();
      it->getMeasurementData(edge_pose);
      for (size_t i = 0; i < 7; ++i)
      {
        g2o_file << edge_pose[i] << " ";
      }
      edge_information = it->information();
      for (size_t i = 0; i < edge_information.rows(); ++i)
        for (size_t j = i; j < edge_information.cols(); ++j)
          g2o_file << edge_information(i, j) << " ";
      g2o_file << ((cvo_slam::tracking_result *)(it->userData()))->inn_pre.value << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_post.value  
      << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_pre.num << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_post.num 
       << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_pre.num_e << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_post.num_e << std::endl;    
    }
    g2o_file.close();
  }

  // Debug function for printing the data of all loop-closure edges in the global pose graph, not used by default 
  void KeyframeEdgesPrint()
  {
    std::ofstream g2o_file;

    // Modify the address to your local address
    g2o_file.open("/home/xi/edges_lc.txt");

    double vertex_pose[7];
    double edge_pose[7];
    int pre_id;
    bool abandon = false;

    g2o::EdgeSE3::InformationType edge_information;
    
    for (g2o::OptimizableGraph::EdgeSet::iterator iter = keyframegraph_.edges().begin(); iter != keyframegraph_.edges().end(); ++iter)
    {
      g2o::EdgeSE3 *it = (g2o::EdgeSE3 *)(*iter);
      std::stringstream vertices_id;
      pre_id = 0;
      for (g2o::HyperGraph::VertexContainer::iterator i = (it->vertices()).begin(); i != (it->vertices()).end(); ++i)
      {
        
        if ((*i)->id() > FirstOdometryId || ((*i)->id() - pre_id) == 1)
        {
          abandon = true;
          break;
        }
        vertices_id << (*i)->id() - 1 << " ";

        pre_id = (*i)->id();
      }
      g2o::VertexSE3 *temp_v1 = (g2o::VertexSE3 *)it->vertex(0);
      g2o::VertexSE3 *temp_v2 = (g2o::VertexSE3 *)it->vertex(1);
      vertices_id << ((cvo_slam::Timestamped *)((temp_v1)->userData()))->timestamp << " " << ((cvo_slam::Timestamped *)((temp_v2)->userData()))->timestamp << " ";

      if (abandon)
      {
        abandon = false;
        continue;
      }

      g2o_file << vertices_id.str();
      it->getMeasurementData(edge_pose);
      for (size_t i = 0; i < 7; ++i)
      {
        g2o_file << edge_pose[i] << " ";
      }

      g2o_file << ((cvo_slam::tracking_result *)(it->userData()))->matches << " " << ((cvo_slam::tracking_result *)(it->userData()))->score 
      << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_prior.value << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_lc_prior.value
      << " " << ((cvo_slam::tracking_result *)(it->userData()))->inn_pre.value << " "  << ((cvo_slam::tracking_result *)(it->userData()))->inn_post.value << std::endl;
    }
    g2o_file.close();
  }


  // Variables in tbb implementation for parallel computing, remaining furthur development (Please do not enable multithreading in config.txt)   
  tbb::concurrent_bounded_queue<LocalMap::Ptr> new_keyframes_;
  bool optimization_thread_shutdown_;
  tbb::tbb_thread optimization_thread_;
  tbb::mutex new_keyframe_sync_, queue_empty_sync_;
  

  KeyframeVector keyframes_; 
  short next_keyframe_id_; 
  int next_odometry_vertex_id_, next_odometry_edge_id_;
  int lc_num;

  g2o::SparseOptimizer keyframegraph_;
  
  cvo_slam::cfg cfg_;

  KeyframeGraph *me_;

  boost::signals2::signal<KeyframeGraph::MapChangedCallbackSignature> map_changed_;

  cvo_slam::OrbVocabularyPtr OrbVoc;

  cvo_slam::OrbMatcherPtr OrbMatcher;

  pcl::registration::TransformationEstimationSVD<pcl::PointXYZ,pcl::PointXYZ,double> PointCloudRegistrator;
};
} 

KeyframeGraph::KeyframeGraph(OrbVocabularyPtr& OrbVoc_) : impl_(new internal::KeyframeGraphImpl(OrbVoc_))
{
  impl_->me_ = this;
}

KeyframeGraph::~KeyframeGraph()
{
}

void KeyframeGraph::configure(const cvo_slam::cfg &config)
{
  impl_->configure(config);
}

void KeyframeGraph::add(const LocalMap::Ptr &keyframe)
{
  impl_->add(keyframe);
}

void KeyframeGraph::finalOptimization()
{
  impl_->finalOptimization();
}

void KeyframeGraph::addMapChangedCallback(const KeyframeGraph::MapChangedCallback &callback)
{
  impl_->map_changed_.connect(callback);
}

const g2o::SparseOptimizer &KeyframeGraph::graph() const
{
  return impl_->keyframegraph_;
}

const KeyframeVector &KeyframeGraph::keyframes() const
{
  return impl_->keyframes_;
}

}
