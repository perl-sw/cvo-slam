/**
 * This is a modified version of local_map.cpp from dvo (see below).
 * Changes: 1) add definition of new functions in class LocalMap; 
 *          2) remove some original member functions and variables in class LocalMapImpl; 
 *          3) In class LocalMapImpl, claim new member functions and variables, and add function definitions; 
 *          4) change the namespace from dvo_slam to cvo_slam
 * Date: Nov 2019
 * Xi Lin, Dingyi Sun
 */

/**
 *  This file is part of dvo.
 *
 *  Copyright 2013 Christian Kerl <christian.kerl@in.tum.de> (Technical University of Munich)
 *  For more information see <http://vision.in.tum.de/data/software/dvo>.
 *
 *  dvo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  dvo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with dvo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "local_map.h"

namespace cvo_slam
{

namespace internal
{

cvo_slam::Transform_Vertex toIsometry(const cvo_slam::Transform& pose)
{
  cvo_slam::Transform_Vertex p(pose.rotation());
  p.translation() = pose.translation();

  return p;
}

static cvo_slam::Transform toAffine(const cvo_slam::Transform_Vertex& pose)
{
  cvo_slam::Transform p(pose.rotation());
  p.translation() = pose.translation();

  return p;
}

struct LocalMapImpl
{
  typedef g2o::BlockSolver_6_3 BlockSolver;
  typedef g2o::LinearSolverEigen<BlockSolver::PoseMatrixType> LinearSolver;
  cvo_slam::KeyframePtr keyframe_;
  cvo_slam::Image_Ptr current_;
  g2o::VertexSE3 *keyframe_vertex_, *previous_vertex_, *current_vertex_;

  g2o::SparseOptimizer graph_;
  int max_vertex_id_, max_edge_id_;

  bool last_map;

  cvo_slam::cfg cfg_;

  // Only used in the last local map
  cvo_slam::KeyframePtr last_keyframe_;

  LocalMapImpl(cvo_slam::KeyframePtr& keyframe, const Transform& cvo_keyframe_pose, const cvo_slam::cfg& cfg) :
    keyframe_ (keyframe),
    cfg_ (cfg),
    keyframe_vertex_(0),
    previous_vertex_(0),
    current_vertex_(0),
    max_vertex_id_(1),
    max_edge_id_(1),
    last_map(false)
  {

    // g2o version used here is 20170730 (https://github.com/RainerKuemmerle/g2o/releases), can not use a newer version 
    graph_.setAlgorithm(
        new g2o::OptimizationAlgorithmLevenberg(
            new BlockSolver(
                new LinearSolver()
            )
        )
    );
            
    graph_.setVerbose(false);

    keyframe_vertex_ = addFrameVertex(keyframe->timestamp);
    keyframe_vertex_->setFixed(true);
    keyframe_vertex_->setEstimate(toIsometry(cvo_keyframe_pose));

  }

  g2o::VertexSE3* addFrameVertex(const ros::Time& timestamp)
  {
    g2o::VertexSE3* frame_vertex = new g2o::VertexSE3();
    frame_vertex->setId(max_vertex_id_++);
    frame_vertex->setUserData(new cvo_slam::Timestamped(timestamp));

    if(!graph_.addVertex(frame_vertex))
    {
      throw std::runtime_error("failed to add vertex to g2o graph!");
    }

    return frame_vertex;
  }

  g2o::EdgeSE3* addTransformationEdge(g2o::VertexSE3 *from, g2o::VertexSE3 *to, const cvo_slam::tracking_result &result)
  {
    assert(from != 0 && to != 0);

    g2o::EdgeSE3* edge = new g2o::EdgeSE3();
    edge->setId(max_edge_id_++);
    edge->resize(2);
    edge->setVertex(0, from);
    edge->setVertex(1, to);
    edge->setMeasurement(toIsometry(result.transform));
    edge->setInformation(result.information);
    edge->setUserData(new cvo_slam::tracking_result(result));
    edge->setRobustKernel(createRobustKernel());

    graph_.addEdge(edge);

    return edge;
  }

  g2o::RobustKernel* createRobustKernel()
  {
    if (cfg_.UseRobustKernel)
    {
      g2o::RobustKernel *k = new g2o::RobustKernelCauchy();
      k->setDelta(cfg_.RobustKernelDelta);

      return k;
    }
    else
    {
      return 0;
    }
  }
};

}

LocalMap::Ptr LocalMap::create(KeyframePtr& keyframe, const Transform& cvo_keyframe_pose, const cvo_slam::cfg& cfg)
{
  LocalMap::Ptr result(new LocalMap(keyframe,cvo_keyframe_pose,cfg));
  return result;
}

LocalMap::LocalMap(KeyframePtr& keyframe, const Transform& cvo_keyframe_pose, const cvo_slam::cfg& cfg) :
    impl_(new internal::LocalMapImpl(keyframe, cvo_keyframe_pose, cfg))
{
}

LocalMap::~LocalMap()
{
}

KeyframePtr LocalMap::getKeyframe()
{
  return impl_->keyframe_;
}

Image_Ptr LocalMap::getCurrentFrame()
{
  return impl_->current_;
}

void LocalMap::getCurrentFramePose(Transform& current_pose)
{
  current_pose = getCurrentFramePose();
}

void LocalMap::setKeyframePose(const Transform& keyframe_pose)
{
  impl_->keyframe_vertex_->setEstimate(internal::toIsometry(keyframe_pose));

  g2o::OptimizableGraph::EdgeSet& edges = impl_->keyframe_vertex_->edges();

  for(g2o::OptimizableGraph::EdgeSet::iterator it = edges.begin(); it != edges.end(); ++it)
  {
    g2o::EdgeSE3 *e = (g2o::EdgeSE3*)(*it);

    assert(e->vertex(0) == impl_->keyframe_vertex_);

    g2o::VertexSE3 *v = (g2o::VertexSE3*)e->vertex(1);
    v->setEstimate(impl_->keyframe_vertex_->estimate() * e->measurement());
  }
}

Transform LocalMap::getCurrentFramePose()
{
  return internal::toAffine(impl_->current_vertex_->estimate());
}

g2o::SparseOptimizer& LocalMap::getGraph()
{
  return impl_->graph_;
}

void LocalMap::addFrame(const Image_Ptr& frame)
{
  impl_->current_ = frame;
  impl_->previous_vertex_ = impl_->current_vertex_;
  impl_->current_vertex_ = impl_->addFrameVertex(frame->timestamp);
}

void LocalMap::addOdometryMeasurement(const cvo_slam::tracking_result &result)
{
  impl_->addTransformationEdge(impl_->previous_vertex_, impl_->current_vertex_, result);
}

void LocalMap::addKeyframeMeasurement(const cvo_slam::tracking_result &result)
{
  impl_->addTransformationEdge(impl_->keyframe_vertex_, impl_->current_vertex_, result);
  impl_->current_vertex_->setEstimate(impl_->keyframe_vertex_->estimate() * internal::toIsometry(result.transform));
}

void LocalMap::optimize()
{
  impl_->graph_.initializeOptimization();
  impl_->graph_.computeInitialGuess();
  impl_->graph_.optimize(50);
}

void LocalMap::setLastMap()
{
  impl_->last_map = true;
}

bool LocalMap::lastMap()
{
  return impl_->last_map;
}

void LocalMap::setLastKeyframe(cvo_slam::KeyframePtr& keyframe)
{
  impl_->last_keyframe_ = keyframe;
}

KeyframePtr LocalMap::getLastKeyframe()
{
  return impl_->last_keyframe_;
}

}
