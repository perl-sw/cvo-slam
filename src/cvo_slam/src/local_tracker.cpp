/**
 * This is a modified version of local_tracker.cpp from dvo (see below).
 * Changes: 1) add definition of new functions in class LocalTracker; 
 *          2) remove some original member functions and variables in class LocalTrackerImpl; 
 *          3) In class LocalTrackerImpl, claim new member functions and variables, and add function definitions; 
 *          4) change the namespace from dvo_slam to cvo_slam
 * Date: Nov 2019
 * Xi Lin, Dingyi Sun
 */

/**
 *  This file is part of dvo.
 *
 *  Copyright 2013 Christian Kerl <christian.kerl@in.tum.de> (Technical University of Munich)
 *  For more information see <http://vision.in.tum.de/data/software/dvo>.
 *
 *  dvo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  dvo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with dvo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "local_tracker.h"

namespace cvo_slam
{

namespace internal
{

struct LocalTrackerImpl
{
  friend class LocalTracker;

  LocalTrackerImpl(OrbVocabularyPtr& OrbVoc, const string& strSettingsFile)
  {
    cvo_keyframe.reset(new cvo::rkhs_se3());
    cvo_odometry.reset(new cvo::rkhs_se3());
    new_map = false;
    last_keyframe_pose_.setIdentity();
    force_ = false;

    ORBvocabulary = OrbVoc;

    /** 
    * Here we reference to ORB-SLAM2 by Raúl et al.
    * https://github.com/raulmur/ORB_SLAM2/blob/master/src/Tracking.cc#L53-150
    **/

    // Read the configuration file for ORB features
    cv::FileStorage fSettings(strSettingsFile, cv::FileStorage::READ);

    float fx = fSettings["Camera.fx"];
    float fy = fSettings["Camera.fy"];
    float cx = fSettings["Camera.cx"];
    float cy = fSettings["Camera.cy"];

    intrinsic = cv::Mat::eye(3,3,CV_32F);
    intrinsic.at<float>(0,0) = fx;
    intrinsic.at<float>(1,1) = fy;
    intrinsic.at<float>(0,2) = cx;
    intrinsic.at<float>(1,2) = cy;
    
    int nRGB = fSettings["Camera.RGB"];
    mbRGB = nRGB;

    mDepthMapFactor = fSettings["DepthMapFactor"];
    if(fabs(mDepthMapFactor)<1e-5)
        mDepthMapFactor=1;
    else
        mDepthMapFactor = 1.0f/mDepthMapFactor;

    int nFeatures = fSettings["ORBextractor.nFeatures"];
    float fScaleFactor = fSettings["ORBextractor.scaleFactor"];
    int nLevels = fSettings["ORBextractor.nLevels"];
    int fIniThFAST = fSettings["ORBextractor.iniThFAST"];
    int fMinThFAST = fSettings["ORBextractor.minThFAST"];

    ORBextractor.reset(new ORB_SLAM2::ORBextractor(nFeatures,fScaleFactor,nLevels,fIniThFAST,fMinThFAST));
  }

  cvo_slam::CvoPtr cvo_keyframe, cvo_odometry;

  cvo_slam::OrbVocabularyPtr ORBvocabulary;

  cvo_slam::OrbExtractorPtr ORBextractor;

  cv::Mat intrinsic;

  cvo_slam::cfg cfg_;

  bool mbRGB;

  float mDepthMapFactor;

  bool new_map;

  Transform last_keyframe_pose_;

  bool force_;

  LocalTracker::AcceptSignal accept_;
  LocalTracker::MapInitializedSignal map_initialized_;
  LocalTracker::MapCompleteSignal map_complete_;

};
}

LocalTracker::LocalTracker(OrbVocabularyPtr& OrbVoc, const string& strSettingsFile) :
    impl_(new internal::LocalTrackerImpl(OrbVoc, strSettingsFile))
{
}

LocalTracker::~LocalTracker()
{
}

LocalMap::Ptr LocalTracker::getLocalMap() const
{
  return local_map_;
}

void LocalTracker::getCurrentPose(Transform& pose)
{
  local_map_->getCurrentFramePose(pose);
}

boost::signals2::connection LocalTracker::addAcceptCallback(const AcceptCallback& callback)
{
  return impl_->accept_.connect(callback);
}

boost::signals2::connection LocalTracker::addMapCompleteCallback(const MapCompleteCallback& callback)
{
  return impl_->map_complete_.connect(callback);
}

boost::signals2::connection LocalTracker::addMapInitializedCallback(const MapInitializedCallback& callback)
{
  return impl_->map_initialized_.connect(callback);
}

void LocalTracker::initNewLocalMap(const Image_Ptr& keyframe, const Image_Ptr& frame, const Transform& keyframe_pose)
{
  tracking_result r_odometry;
  
  // Initialize the CVO object for consecutive frame tracking with the first frame
  impl_->cvo_odometry->set_pcd(impl_->cfg_.dataset_seq, keyframe->rgb, keyframe->depth);

  // Initialize the CVO object for keyframe based tracking with the first frame
  impl_->cvo_keyframe->set_pcd(impl_->cfg_.dataset_seq, keyframe->rgb, keyframe->depth);

  impl_->cvo_odometry->match_odometry(impl_->cfg_.dataset_seq, frame->rgb, frame->depth, r_odometry.transform);
  impl_->last_keyframe_pose_ = r_odometry.transform;

  Eigen::Affine3f tran = r_odometry.transform.cast<float>();
  impl_->cvo_odometry->compute_innerproduct(r_odometry.inn_pre, r_odometry.inn_post, tran);

  impl_->cvo_odometry->update_fixed_pcd();

  // Currently we do not have effective way to estimate the information of a given transformation. Here we use a rough estimation based on the CVO inner
  // product values before and after registration.  
  r_odometry.information = (r_odometry.inn_post.value > 100 || r_odometry.inn_pre.value > 25) ? 500000 * Information::Identity() : 50000 * Information::Identity();

  initNewLocalMap(keyframe, frame, r_odometry, keyframe_pose);
}

void LocalTracker::initNewLocalMap(const Image_Ptr& keyframe, const Image_Ptr& frame, const tracking_result& r_odometry, const Transform& keyframe_pose)
{
  // Create new keyframe
  KeyframePtr kf(new Keyframe(keyframe, keyframe_pose, impl_->intrinsic, impl_->mbRGB, impl_->mDepthMapFactor));

  // Extract ORB features
  impl_->ORBextractor->ExtractOrb(kf->ImGray,kf->keypoints,kf->descriptors);

  // Compute ORB feature vector and BoW vector
  std::vector<cv::Mat> DesVec = ORB_SLAM2::Converter::toDescriptorVector(kf->descriptors);
  impl_->ORBvocabulary->transform(DesVec,kf->BowVec, kf->FeatVec,4);

  local_map_ = LocalMap::create(kf,keyframe_pose,impl_->cfg_);
  local_map_->addFrame(frame);

  std::cout << "Initialize a new local map" << std::endl << std::endl; 
  if(impl_->cvo_keyframe->first_frame){
      impl_->cvo_keyframe->first_frame = false;
      Eigen::Affine3f transform = r_odometry.transform.cast<float>();
      impl_->cvo_keyframe->reset_transform(transform);
  }
  else {
      Eigen::Affine3f transform = r_odometry.transform.cast<float>();
      impl_->cvo_keyframe->reset_keyframe(transform);
      impl_->new_map = true;
  }

  local_map_->addKeyframeMeasurement(r_odometry);

  impl_->map_initialized_(*this, local_map_, r_odometry);
}

void LocalTracker::update(const Image_Ptr& image, Transform& pose)
{
  if(impl_->new_map) impl_->new_map = false;
  
  tracking_result r_odometry, r_keyframe;

  // Perform consecutive tracking
  impl_->cvo_odometry->match_odometry(impl_->cfg_.dataset_seq, image->rgb, image->depth, r_odometry.transform);
  Eigen::Affine3f tran = r_odometry.transform.cast<float>();
  impl_->cvo_odometry->compute_innerproduct(r_odometry.inn_pre, r_odometry.inn_post, tran);
  impl_->cvo_odometry->update_fixed_pcd();

  // Perform keyframe-based tracking
  Eigen::Affine3f transform = r_odometry.transform.cast<float>();
  impl_->cvo_keyframe->reset_initial(transform);
  impl_->cvo_keyframe->match_keyframe(impl_->cfg_.dataset_seq, image->rgb, image->depth, r_keyframe.transform);
  
  tran = r_keyframe.transform.cast<float>();
  impl_->cvo_keyframe->compute_innerproduct(r_keyframe.inn_pre, r_keyframe.inn_post, tran);


  r_odometry.information = (r_odometry.inn_post.value > 100 || r_odometry.inn_pre.value > 25) ? 500000 * Information::Identity() : 50000 * Information::Identity();
  r_keyframe.information = (r_keyframe.inn_post.value > 100 || r_keyframe.inn_pre.value > 25) ? 500000 * Information::Identity() : 50000 * Information::Identity();
 
  // Check if a new keyframe is needed
  std::cout << "Check whether a new keyframe is needed" << std::endl;  
  if(impl_->accept_(*this, r_odometry, r_keyframe) && !impl_->force_)
  {
    std::cout << "Update current local pose graph" << std::endl << std::endl;
    local_map_->addFrame(image);
    local_map_->addOdometryMeasurement(r_odometry);
    local_map_->addKeyframeMeasurement(r_keyframe);

    impl_->cvo_keyframe->update_previous_pcd();

    impl_->last_keyframe_pose_ = r_keyframe.transform;
  }
  else
  {
    std::cout << "Current local pose graph completes" << std::endl << std::endl;
    Transform current_pose = local_map_->getCurrentFramePose();
    
    impl_->map_complete_(*this, local_map_);

    initNewLocalMap(local_map_->getCurrentFrame(), image, r_odometry, current_pose);

    impl_->last_keyframe_pose_ = r_odometry.transform;

    // If the current frame is the final frame, force the current local pose graph to complete
    if(impl_->force_){
      local_map_->setLastMap();

      KeyframePtr kf(new Keyframe(image, local_map_->getCurrentFramePose(), impl_->intrinsic, impl_->mbRGB, impl_->mDepthMapFactor));

      impl_->ORBextractor->ExtractOrb(kf->ImGray,kf->keypoints,kf->descriptors);

      std::vector<cv::Mat> DesVec = ORB_SLAM2::Converter::toDescriptorVector(kf->descriptors);
      impl_->ORBvocabulary->transform(DesVec,kf->BowVec, kf->FeatVec,4);

      local_map_->setLastKeyframe(kf);

      impl_->map_complete_(*this, local_map_);
    }   

  }

  local_map_->getCurrentFramePose(pose);    
}

bool LocalTracker::checkNewMap()
{
  return impl_->new_map;
}

void LocalTracker::forceCompleteCurrentLocalMap()
{
  impl_->force_ = true;
}

void LocalTracker::configure(const cfg& config)
{
  impl_->cfg_ = config;
}

}
