/**
 * This is a modified version of map_serializer.h from dvo (see below).
 * Changes: remove definition of some subclasses of MapSerializerInterface.
 * Date: Nov 2019
 * Xi Lin, Dingyi Sun
 */

/**
 *  This file is part of dvo.
 *
 *  Copyright 2012 Christian Kerl <christian.kerl@in.tum.de> (Technical University of Munich)
 *  For more information see <http://vision.in.tum.de/data/software/dvo>.
 *
 *  dvo is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  dvo is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with dvo.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "map_serializer.h"


namespace cvo_slam
{

MapSerializerInterface::MapSerializerInterface()
{
}

MapSerializerInterface::~MapSerializerInterface()
{
}

TrajectorySerializer::TrajectorySerializer(std::ostream& stream) :
    stream_(stream)
{
}

TrajectorySerializer::~TrajectorySerializer()
{
}

void TrajectorySerializer::serialize(const KeyframeGraph::Ptr& map)
{
  std::map<ros::Time, Transform_Vertex> poses;

  for (g2o::OptimizableGraph::VertexIDMap::const_iterator it = map->graph().vertices().begin(); it != map->graph().vertices().end(); ++it)
  {
    g2o::VertexSE3 *v = (g2o::VertexSE3 *) it->second;

    Timestamped *t = dynamic_cast<Timestamped *>(v->userData());

    assert(t != 0);

    poses[t->timestamp] = v->estimate();
  }

  for (std::map<ros::Time, Transform_Vertex>::iterator it = poses.begin(); it != poses.end(); ++it)
  {
    Eigen::Quaterniond q(it->second.rotation());

    stream_ << it->first << " " << it->second.translation()(0) << " " << it->second.translation()(1) << " " << it->second.translation()(2) << " " << q.x() << " " << q.y() << " " << q.z() << " " << q.w() << " " << std::endl;
  }
}

}
