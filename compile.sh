#!/bin/bash

target_dir=$1

if [ -d "$target_dir" ]; then
    rm -r $target_dir
fi

mkdir $target_dir

cp -r ./* $target_dir
cd $target_dir
catkin_make -j$(expr $(nproc) - 4) -DCMAKE_BUILD_TYPE=Release -DCMAKE_C_COMPILER=icc -DCMAKE_CXX_COMPILER=icpc


